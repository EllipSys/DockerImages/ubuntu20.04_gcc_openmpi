FROM ubuntu:20.04

MAINTAINER Frederik Zahle <frza@dtu.dk>

ENV DEBIAN_FRONTEND=noninteractive

RUN apt-get update \
 && apt-get install --fix-missing -y -q \
    gfortran \
    git-all \
    wget \
    curl \
    build-essential \
    python3.8-venv \
    openmpi-bin openmpi-common libopenmpi-dev \
 && apt-get autoremove -y \
 && apt-get clean -y



